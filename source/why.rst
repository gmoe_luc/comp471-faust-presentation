.. _MATLAB: http://www.mathworks.com/products/matlab/
.. _GNU Octave: http://en.wikipedia.org/wiki/GNU_Octave
.. _SuperCollider: https://supercollider.github.io/
.. _Max: http://cycling74.com/products/max/

Why Faust?
==========

* Free and open source vs. `MATLAB`_
* Specialized for audio streams vs. `GNU Octave`_
* Can match or outperform native C++

Great operating system support
------------------------------

* Windows
* OSX
* Linux
* Android / iOS 
  
Great platform support
----------------------

* LADSPA
* Virtual Studio Technology / Audio Units
* SuperCollider
* Csound
* ...and many more!
