Faust Presentation - Index
==========================

.. _Faust: http://faust.grame.fr/
.. _Loyola University Chicago: http://www.luc.edu/
.. _Griffin Moe: http://griffinmoe.com/
.. _Sphinx: http://sphinx-doc.org/index.html
.. _ReadTheDocs: https://readthedocs.org/

This is a presentation on the `Faust`_ programming language for `Loyola
University Chicago`_'s COMP372/471 Programming Langauges course. It 
was created by `Griffin Moe`_ using the `Sphinx`_ documentation tool 
and is hosted on `ReadTheDocs`_.

.. toctree::
   :maxdepth: 2

   intro
   why
   language
   examples
   criteria
   conclusion
   resources
