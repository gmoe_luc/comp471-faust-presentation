Evaluation
==========

* **Readability (4/5)** - Better than other specification langauges
* **Productivity (4/5)** - No escaping C++...  
* **Community (3/5)** - Around since 2002, lack of excitement
* **Major Projects:**

  * `Guitarix <http://faust.grame.fr/index.php/related-projects/guitarix>`_
  * `Faust -> FPGA <http://www.dafx.ca/proceedings/papers/p_287.pdf>`_
  * `Recent Academic Papers <http://faust.grame.fr/index.php/documentation/papers>`_
  * No commercial use (?)

* **Ecosystem (4/5)** - Neat tools available
* **Coolness (4/5)** - Excellent abstraction, C++ sucks...
