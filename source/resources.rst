Resources
=========

Tutorials
---------

* `Julius O. Smith's (CCRMA) Faust Tutorial <https://ccrma.stanford.edu/~jos/aspf/aspf.pdf>`_ -
  The best tutorial available in my opinion, Smith has also published some excellent textbooks as well.
* `GRAME Faust Tutorials <http://faust.grame.fr/index.php/documentation/tutorials>`_ -
  These are GRAME's tutorials on Faust, has more of a mathematical leaning than Smith's tutorial.
* `Faust Reference <http://faust.grame.fr/index.php/documentation/references>`_ -
  Who doesn't love a good manual?

DSP Resources
-------------

* `Musimathics - Volume 2 <http://www.amazon.com/Musimathics-Mathematical-Foundations-Music-Volume/dp/026251656X/ref=sr_1_1?ie=UTF8&qid=1397569364&sr=8-1&keywords=musimathics+volume+2>`_ -
  I absolutely recommend this book if you are interested in learning signal processing. It assumes very little of the reader, very easy to read.
* `MusicDSP.org <http://www.musicdsp.org/>`_ - 
  Contains a list of code examples of various algorithms in a variety of langauges, including C/C++, MATLAB, and Delphi.
* `The Scientist and Engineer's Guide to Digital Signal Processing <http://www.dspguide.com/pdfbook.htm>`_ -
  A free textbook covering DSP in fantastic detail. Fairly easy read as well, though more geared towards digital communications.

Faust Tools
-----------

* `FauseWorks <http://faust.grame.fr/index.php/related-projects/faust-works>`_ - 
  A Faust IDE that displays Faust and transpiled C++ code, and automatically generates signal path block diagrams.
* `Vim-Faust <https://github.com/gmoe/vim-faust>`_ -
  An open source syntax-highlighting plugin for Vim, developed by yours truly!
