The Language
============

* Purely functional language
* Faust compiler transpiles to C++ code
* Built-in UI widgets
* No run-time libraries required!
* Block-diagram syntax
* Intrinisically signal-based:

  1. Digital signals - discrete time functions
  2. Signal processor - Operate functions on signals (2nd order)
  3. Composition operators - Ties processors together (3rd order)

Signal Processor Examples
-------------------------

Generates silence::

  process = 0;

Passes input signal to output, a cable::

  process = _;

Downmixes stereo signal to mono::

  process = +;

Processor Composition
---------------------

We combine discrete processors using the following operators:

+------+-------------------------+
| Oper | Operator Function       |
+======+=========================+
| f~g  |  Recursive composition  | 
+------+-------------------------+
| f,g  |  Parallel composition   | 
+------+-------------------------+
| f:g  |  Sequential composition | 
+------+-------------------------+
| f<:g |  Split composition      | 
+------+-------------------------+
| f:>g |  Merge composition      |
+------+-------------------------+

Similar to our second example in the section above, a stereo cable::

  process = _,_;

Resursion used to create a one sample delay::

  //Y(t) = X(t) + Y(t−1)
  process = + ~ _;
