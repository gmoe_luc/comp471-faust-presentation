Conclusion
==========

* Great environment for test algorithms without C++
* Free and open source, Faust > Max or MATLAB
* Transpiles to C++
* Support for SuperCollider, Max, Pure Data, etc.
* Block diagram visualizations
* Generates VST and AU plug-ins!!!
