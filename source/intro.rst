Introduction to Faust
=====================

.. _Faust: http://faust.grame.fr/
.. _GRAME Centre National de Creation Musicale: http://www.grame.fr/

* `Faust`_ - Functional Audio Stream 
* Real-time signal processing and synthesis
* GPL licensed 
* Developed by `GRAME Centre National de Creation Musicale`_

.. figure:: noise_diagram.png
