Examples
========

Noise Generator - Faust Code
----------------------------
::

  declare name        "noise";
  declare version     "1.0";
  declare author      "Grame";
  declare license     "BSD";
  declare copyright   "(c) GRAME 2006";

  // noise generator

  random  = +(12345)~*(1103515245);
  noise   = random/2147483647.0;

  process = noise * vslider("vol", 0, 0, 1, 0.1);

Noise Generator - C++ Code
--------------------------

.. code-block:: cpp

  //-----------------------------------------------------
  // author: "Grame"
  // copyright: "(c) GRAME 2006"
  // license: "BSD"
  // name: "noise"
  // version: "1.0"
  //
  // Code generated with Faust 2.0.a21 (http://faust.grame.fr)
  //-----------------------------------------------------
  #ifndef FAUSTFLOAT
  #define FAUSTFLOAT float
  #endif  



  #ifndef FAUSTCLASS 
  #define FAUSTCLASS mydsp
  #endif

  class mydsp : public dsp {
          
    private:
          
          int iRec0[2];
          FAUSTFLOAT fvslider0;
          int fSamplingFreq;
          
    public:
          
          void static metadata(Meta* m) { 
                  m->declare("author", "Grame");
                  m->declare("copyright", "(c) GRAME 2006");
                  m->declare("license", "BSD");
                  m->declare("name", "noise");
                  m->declare("version", "1.0");
          }

          virtual int getNumInputs() {
                  return 0;
                  
          }
          virtual int getNumOutputs() {
                  return 1;
                  
          }
          virtual int getInputRate(int channel) {
                  int rate;
                  switch (channel) {
                          default: {
                                  rate = -1;
                                  break;
                          }
                          
                  }
                  return rate;
                  
          }
        virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceInit(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		fvslider0 = FAUSTFLOAT(0.);
		for (int i = 0; (i < 2); i = (i + 1)) {
			iRec0[i] = 0;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	
	virtual void buildUserInterface(UI* interface) {
		interface->openVerticalBox("noise");
		interface->addVerticalSlider("vol", &fvslider0, 0.f, 0.f, 1.f, 0.1f);
		interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* output0 = outputs[0];
		float fSlow0 = (4.65661e-10f * float(fvslider0));
		for (int i = 0; (i < count); i = (i + 1)) {
			iRec0[0] = (12345 + (1103515245 * iRec0[1]));
			output0[i] = FAUSTFLOAT((fSlow0 * float(iRec0[0])));
			iRec0[1] = iRec0[0];
			
		}
		
	}
  };

Noise Generator - Signal Path Diagram
-------------------------------------

.. figure:: noise_diagram.png

For more examples: `<http://faust.grame.fr/index.php/online-examples>`_
